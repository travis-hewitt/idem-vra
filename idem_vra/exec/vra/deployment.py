__contracts__ = ["auto_state", "soft_fail"]

from typing import Any
from typing import Dict


async def get_deployment(hub, ctx, deployment_id, *args, **kwargs) -> Dict[str, Any]:
    '''curl -X GET \
  $url/deployment/api/deployments/$deployment_id?apiVersion=$api_version \
  -H "Authorization: Bearer $access_token" | jq "."'''
    url = f"{hub.exec.vra.URL}/deployment/api/deployments/{deployment_id}"
    ret = await hub.exec.request.json.get(ctx, url=url)
    return {"result": True, "comment": "here you go", "ret": ret}


async def list(hub, ctx, *args, **kwargs) -> Dict[str, Any]:
    """curl -X GET \
        -G --data-urlencode "name=$deployment_name" \
        $url/deployment/api/deployments?apiVersion=$api_version \
        -H "Authorization: Bearer $access_token" | jq "."
    """
    url = f"{hub.exec.vra.URL}/deployment/api/deployments"
    ret = await hub.exec.request.json.get(
        ctx, url=url
    )  # the entire response is saved in ret
    list_of_deployments = []
    name_of_deployments = []

    for deployment in ret.ret.content:
        list_of_deployments.append(deployment)
        name_of_deployments.append(deployment["name"])

    return {"result": True, "comment": "here you go", "ret": list_of_deployments}


async def create(hub, ctx, name: str, *args, **kwargs) -> Dict[str, Any]:
    return {"result": True | False, "comment": "", "ret": None}


async def update(hub, ctx, name: str, *args, **kwargs) -> Dict[str, Any]:
    '''curl -X GET \
  $url/deployment/api/deployments/$deployment_id/actions?apiVersion=$api_version \
  -H "Authorization: Bearer $access_token" | jq "."'''
    return {"result": True | False, "comment": "", "ret": None}


async def get_deployment_actions(
    hub, ctx, deployment_id, *args, **kwargs
) -> Dict[str, Any]:
    url = f"{hub.exec.vra.URL}/deployment/api/deployments/{deployment_id}/actions"
    ret = await hub.exec.request.json.get(ctx, url=url)
    return {"result": True | False, "comment": "", "ret": ret.ret}


async def get_deployment_resources(
    hub, ctx, deployment_id, *args, **kwargs
) -> Dict[str, Any]:
    url = f"{hub.exec.vra.URL}/deployment/api/deployments/{deployment_id}/resources"
    ret = await hub.exec.request.json.get(ctx, url=url)
    return {"result": True | False, "comment": "", "ret": ret.ret}


# # # currently getting 403, need to figure out why # # #
async def delete_deployment(hub, ctx, deployment_id, *args, **kwargs) -> Dict[str, Any]:
    url = f"{hub.exec.vra.URL}/iaas/api/deployments/{deployment_id}"
    ret = await hub.exec.request.json.delete(ctx, url=url)
    return {"result": True | False, "comment": "", "ret": ret.ret}

# # # iaas get deployments # # #
async def get_iaas_deployments(hub, ctx, *args, **kwargs) -> Dict[str, Any]:
    url = f"{hub.exec.vra.URL}/iaas/api/deployments"
    ret = await hub.exec.request.json.get(ctx, url=url)
    return {"result": True | False, "comment": "", "ret": ret.ret}

# # # iaas get machines # # #
async def get_machines(hub, ctx, *args, **kwargs) -> Dict[str, Any]:
    url = f"{hub.exec.vra.URL}/iaas/api/machines"
    ret = await hub.exec.request.json.get(ctx, url=url)
    return {"result": True | False, "comment": "", "ret": ret.ret}

# # # iaas snapshot machine # # #
async def snapshot_machine(hub, ctx, machine_id, *args, **kwargs) -> Dict[str, Any]:
    url = f"{hub.exec.vra.URL}/iaas/api/machines/{machine_id}/operations/snapshots"
    ret = await hub.exec.request.json.post(ctx, url=url)
    return {"result": True | False, "comment": "", "ret": ret.ret}


# # # these signature functions are required but i'd like to keep my verbose versions above # # #


async def delete(hub, ctx, name: str, *args, **kwargs) -> Dict[str, Any]:
    return {"result": True | False, "comment": "", "ret": None}


async def get(hub, ctx, name: str, *args, **kwargs) -> Dict[str, Any]:
    url = f"{hub.exec.vra.URL}/blueprint/api/blueprints"
    ret = await hub.exec.request.json.get(ctx, url=url)
    for cloud_template in ret.ret.content:
        if cloud_template["name"] == name:
            return {"result": True, "comment": "", "ret": cloud_template}
    else:
        return {
            "result": False,
            "comment": "Could not find cloud template!",
            "ret": None,
        }
    print(ret)
