__contracts__ = ["auto_state", "soft_fail"]

from typing import Any
from typing import Dict


async def get(hub, ctx, name: str, *args, **kwargs) -> Dict[str, Any]:
    url = f"{hub.exec.vra.URL}/project-service/api/projects"
    ret = await hub.exec.request.json.get(ctx, url=url)
    for project in ret.ret.content:
        if project["name"] == name:
            return {"result": True, "comment": "", "ret": project}
    else:
        return {"result": False, "comment": "Could not find project!", "ret": None}


async def list(hub, ctx, *args, **kwargs) -> Dict[str, Any]:
    url = f"{hub.exec.vra.URL}/project-service/api/projects"
    ret = await hub.exec.request.json.get(ctx, url=url)
    return ret


async def create(hub, ctx, name: str, *args, **kwargs) -> Dict[str, Any]:
    return {"result": True | False, "comment": "", "ret": None}


async def update(hub, ctx, name: str, *args, **kwargs) -> Dict[str, Any]:
    return {"result": True | False, "comment": "", "ret": None}


async def delete(hub, ctx, name: str, *args, **kwargs) -> Dict[str, Any]:
    return {"result": True | False, "comment": "", "ret": None}
