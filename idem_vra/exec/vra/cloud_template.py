__contracts__ = ["auto_state", "soft_fail"]

from typing import Any
from typing import Dict

# # # Need to be able to deploy instances of blueprints based on blueprint_id, inputs, desired name of deployment # # #
# # # Need to be able to get deployment status # # #
# # # Need to be able to collect and interact with ips of all the VMs created from deployment # # #
# # # Need to be able to manage deployment given a deployment id (basically, want to be able to snapshot/delete to clean up) # # #
# # # Need to be able to use this as a jenkins worker or gitlab runner, so however to generate the fernet.yaml creds from a service account # # #


async def get(hub, ctx, name: str, *args, **kwargs) -> Dict[str, Any]:
    url = f"{hub.exec.vra.URL}/blueprint/api/blueprints"
    ret = await hub.exec.request.json.get(ctx, url=url)
    for cloud_template in ret.ret.content:
        if cloud_template["name"] == name:
            return {"result": True, "comment": "", "ret": cloud_template}
    else:
        return {
            "result": False,
            "comment": "Could not find cloud template!",
            "ret": None,
        }
    print(ret)


async def list(hub, ctx, *args, **kwargs) -> Dict[str, Any]:
    url = f"{hub.exec.vra.URL}/blueprint/api/blueprints"
    ret = await hub.exec.request.json.get(ctx, url=url)
    return ret


async def deploy(
    hub, ctx, blueprint_id, deployment_name, inputs, simulate=False, *args, **kwargs
) -> Dict[str, Any]:
    """ """
    """
    curl - X POST \
        $url/blueprint/api/blueprint-requests?apiVersion=$api_version\
    - H "Authorization: Bearer $access_token" \
    - H 'Content-Type: application/json' \
    - d '{
        "description": "requesting deployment from cloud template",
        "blueprintId": "'"$cloud_template_id"'",
        "inputs": {
                "count": 2,
            "image": "'"$image_mapping"'",
            "flavor": "'"$flavor_mapping"'"
        }
    }' | jq "."

    """
    """
    """

    url = f"{hub.exec.vra.URL}/blueprint/api/blueprint-requests"
    ret = await hub.exec.request.json.post(
        ctx,
        url=url,
        json={
            "simulate": False,
            "blueprintId": blueprint_id,
            "inputs": inputs,
            "deploymentName": deployment_name,
        },
    )
    return {"result": True | False, "comment": "", "ret": ret.ret}

async def get_blueprint_by_id(hub, ctx, blueprint_id, *args, **kwargs) -> Dict[str, Any]:
    url = f"{hub.exec.vra.URL}/blueprint/api/blueprints/{blueprint_id}"
    ret = await hub.exec.request.json.get(ctx, url=url)
    return {"result": True, "comment": "here you go", "ret": ret}

async def update(hub, ctx, name: str, *args, **kwargs) -> Dict[str, Any]:
    return {"result": True | False, "comment": "", "ret": None}


async def delete(hub, ctx, name: str, *args, **kwargs) -> Dict[str, Any]:
    return {"result": True | False, "comment": "", "ret": None}


async def create(hub, ctx, name: str, *args, **kwargs) -> Dict[str, Any]:
    return {"result": True | False, "comment": "", "ret": None}
