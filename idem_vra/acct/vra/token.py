import dict_tools.data as data

# https://console.cloud.vmware.com/csp/gateway/am/api/swagger-ui.html
MANAGEMENT_URL = "https://api.mgmt.cloud.vmware.com"


async def gather(hub, profiles):
    """

    Example:

    .. code-block:: yaml

        csp.token:
          profile_name:
            refresh_token: dmd23q3au8ljyajcvhz207of4ivsn9vjiaxzez223qeagdpe0voqiasknykv58jt
            api_version: '2020-08-25'

    """

    sub_profiles = {}
    for profile, ctx in profiles.get("vra.token", {}).items():
        ctx.api_version = ctx.get("api_version")
        token = ctx.get("refresh_token") or ctx.get("token")
        token_data = await hub.exec.request.json.post(
            data.NamespaceDict(acct={}),
            url=f"{MANAGEMENT_URL}/iaas/api/login",
            raise_for_status=True,
            data=f'{{"refreshToken": "{token}"}}',
        )
        if not token_data:
            continue

        ctx.headers = {
            "Authorization": f"Bearer {token_data.ret.token}",
        }

        sub_profiles[profile] = ctx

    return sub_profiles
